import os
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException, NoSuchElementException, StaleElementReferenceException
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class Browser(object):

    def __init__(self, driver):
        self.driver = driver

    def click_button(self, selector):
        element = self.get_element(selector)
        print("Click the button")
        element.click()

    def open_web_page(self, driver, url):
        print("Open web page: {0}".format(url))
        driver.get(url)
        print("Web page has been opened.")

    def wait_till_element_is_displayed(self, driver, selector, timeout=15):
        """
        :return: True if element IS displayed
        """
        wait = WebDriverWait(driver, timeout=int(timeout),
                             ignored_exceptions=(StaleElementReferenceException, NoSuchElementException))
        try:
            wait.until(EC.presence_of_element_located((By.XPATH, selector)))
            return True
        except (TimeoutException, NoSuchElementException):
            raise TimeoutException("Timeout waiting for element", selector)

    def get_element(self, selectors):
        if isinstance(selectors, list):
            element = self._findDeeperElement(self.driver, selectors[0])
            index = 1
            while index < len(selectors):
                element = self._findDeeperElement(element, selectors[index])
                index += 1
            return element
        else:
            return self._findDeeperElement(self.driver, selectors)

    def _findDeeperElement(self, parentElement, singleSelector):
        if (singleSelector[0] == '/') or (singleSelector[0] == '*'):
            return parentElement.find_element_by_xpath(singleSelector)
        else:
            return parentElement.find_element_by_css_selector(singleSelector)
import unittest
from selenium import webdriver
from pages.AuthorizationPage import AuthorizationPage
from pages.MainPage import MainPage
from Utils.browser import Browser

class TestAuthorization(unittest.TestCase):
    """A test case for authorization"""

    def setUp(self):
        # create a new Firefox session
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()
        # navigate to the application home page
        self.driver.get("https://104.ua/ru/")

    def testAuthorization(self):
        browser = Browser(self.driver)
        main_page = MainPage()
        browser.wait_till_element_is_displayed(self.driver, main_page.getSelectorBtnVoiti())
        browser.click_button(main_page.getSelectorBtnVoiti())
        authorization_page = AuthorizationPage()
        browser.wait_till_element_is_displayed(self.driver, authorization_page.getSelectorInputUsername())

    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main(verbosity=2)
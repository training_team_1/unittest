from pages.CommonPage import CommonPage
from selenium.webdriver.common.by import By

class RegistrationPage(CommonPage):

    _selector_btn_otkazatsa = "//a[text()='{0} ']".format("Отказаться")
    _selector_btn_prodolzhyt = "//a[text()='{0} ']".format("Продолжить")

    def getSelectorBtnOtkazatsa(self):
        return self._selector_btn_otkazatsa

    def getSelectorBtnProdolzhyt(self):
        return self._selector_btn_prodolzhyt



from pages.CommonPage import CommonPage
from selenium.webdriver.common.by import By

class MainPage(CommonPage):

    _selector_email = "//input[@id='subscribe_email']"

    def getSelectorInputSubscribeEmail(self):
        return self._selector_email

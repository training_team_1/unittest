from pages.CommonPage import CommonPage

class InfoPage(CommonPage):

    _selector_btn_kabinet = "//a[text()='{0}']".format("Кабинет")

    def getSelectorCabinet(self):
        return self._selector_btn_kabinet

class CommonPage(object):
    """Base class that all page models can inherit from"""

    _selector_input_search = "//input[@id='short_q']"
    _selector_btn_registration = "//a[text()='{0}']".format("Регистрация")
    _selector_btn_voiti = "//a[text()='{0}']".format("Войти")

    def getSelectorBtnRegistration(self):
        return self._selector_btn_registration

    def getSelectorBtnVoiti(self):
        return self._selector_btn_voiti

    def getSelectorInputSearch(self):
        return self._selector_input_search
from pages.CommonPage import CommonPage

class AuthorizationPage(CommonPage):

    _selector_input_password = "//input[@id='password']"
    _selector_input_username = "//input[@id='username']"
    _selector_btn_voiti = "//input[@id='form_login_check_submit']"

    def getSelectorInputPassword(self):
        return self._selector_input_password

    def getSelectorInputUsername(self):
        return self._selector_input_username

    def getSelectorInputVoiti(self):
        return self._selector_btn_voiti